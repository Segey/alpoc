DROP USER IF EXISTS camunda;

CREATE USER camunda WITH PASSWORD 'camunda';
ALTER USER camunda WITH SUPERUSER;
SET ROLE camunda;

DROP DATABASE IF EXISTS camunda;
CREATE DATABASE camunda;
GRANT ALL PRIVILEGES ON DATABASE camunda TO camunda;


CREATE schema if NOT EXISTS apl_rf;

DROP TABLE APL_RF.PARTS;
DROP TABLE APL_RF.FIELDS;
DROP table apl_rf.pages;

create table apl_rf.pages(
    id              SERIAL PRIMARY KEY,
    key             VARCHAR NOT NULL UNIQUE,
    name            VARCHAR NOT NULL,
    description     varchar,
    created_at      timestamp default CURRENT_TIMESTAMP,
    updated_at      timestamp default CURRENT_TIMESTAMP,
    type            varchar NOT NULL
);

create table apl_rf.parts(
    id              SERIAL PRIMARY KEY,
    key             VARCHAR NOT NULL,
    content         VARCHAR NOT NULL,
    page_id         integer REFERENCES apl_rf.pages(id)
);

create table apl_rf.fields(
     id              SERIAL PRIMARY KEY,
     key             VARCHAR NOT NULL,
     name            VARCHAR NOT NULL,
     apl_type        VARCHAR NOT NULL,
     page_id         integer REFERENCES apl_rf.pages(id)
);

