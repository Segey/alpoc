import api from '@/api'

const state = {
    processes: [],
}

const getters = {
    processes: (state) => state.processes,
}

const mutations = {
    set_processes: (state, processes) => state.processes = processes,
}

const actions = {

    loadAll({commit}) {
        return api.processes.readAll()
            .then(processes => commit('set_processes', _.orderBy(processes, 'title')))
    },

    load({commit}, id) {
        return api.processes.read(id)
    },

    create({rootGetters}, {key, title}) {
        const currentProject = rootGetters['projects/selected']
        return api.processes.create(key, title, currentProject.id)
    },

    updateTitle({dispatch}, {id, key, title}) {
        return dispatch('load', id)
            .then(({content, project}) => api.processes.update(id, key, title, content, project))
    },

    saveContent({dispatch }, {id, content}) {
        return dispatch('load', id)
            .then(({key, title, project}) => api.processes.update(id, key, title, content, project))
    },

    delete({}, {id}) {
        return api.processes.delete(id)
    },

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
