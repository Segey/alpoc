import api from '@/api'

const state = {
    pages: [],
}

const getters = {
    pages: (state) => state.pages,
}

const mutations = {
    set_pages: (state, pages) => state.pages = pages,
}

const actions = {

    loadAll({commit, getters}) {
        return api.pages.readAll()
            .then(pages => commit('set_pages', _.orderBy(pages, 'title')))
    },

    create({rootGetters}, {key, title}) {
        const currentProject = rootGetters['projects/selected']
        return api.pages.create(key, title, currentProject.id)
    },

    update({rootGetters}, {id, key, title}) {
        const currentProject = rootGetters['projects/selected']
        return api.pages.update(id, key, title, currentProject.id)
    },

    delete({}, {id}) {
        return api.pages.delete(id)
    },

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
