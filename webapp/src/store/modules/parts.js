import api from '@/api'

const state = {
    parts: [],
    parts_by_name: {},
}

const getters = {
    parts: state => state.parts,
    parts_by_name: state => state.parts_by_name,
}

const mutations = {
    set_parts: (state, parts) => {
        state.parts = parts;
        state.parts_by_name = _.keyBy(parts, 'name');
        console.log('set_parts', state.parts_by_name)
    },
    add_part: (state, part) => {
        state.parts.push(part);
        state.parts_by_name = _.keyBy(state.parts, 'name');
        console.log('add_part', state.parts_by_name)
    },
}

const actions = {

    load({commit, getters}, {page_id}) {
        return api.parts.readByPage(page_id)
            .then(parts => {
                commit('set_parts', parts);
                return getters.parts_by_name
            })
    },

    save({commit, getters, rootGetters}, {page_id, name, content}) {

        const part = getters.parts_by_name[name]
        if (part)
            return api.parts.update(part.id, page_id, name, content)
        else
            return api.parts.create(page_id, name, content).then(part => commit('add_part', part))
    },

    create({rootGetters}, {key, title}) {
        // const currentProject = rootGetters['projects/selected']
        // return api.pages.create(key, title, currentProject.id)
    },

    update({rootGetters}, {id, key, title}) {
        // const currentProject = rootGetters['projects/selected']
        // return api.pages.update(id, key, title, currentProject.id)
    },

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
