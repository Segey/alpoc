import api from '@/api'

const state = {
    projects: [],
    selected: null,
}

const getters = {
    projects: (state) => state.projects,
    selected: (state) => state.selected,
}

const mutations = {
    set_projects: (state, projects) => state.projects = projects,
    set_selected: (state, selected) => state.selected = selected,
}

const actions = {

    rebuild({state}) {
        return api.builder.rebuild(state.selected.id)
    },

    loadAll({commit, state}) {
        return api.projects.readAll()
            .then(pages => {
                let orderBy = _.orderBy(pages, 'title');
                commit('set_projects', orderBy);
                if (orderBy.length && !state.selected)
                    commit('set_selected', orderBy[0])

                console.log('pro', orderBy)
            })
    },

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
