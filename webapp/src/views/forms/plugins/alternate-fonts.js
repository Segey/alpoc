import grapes from "grapesjs";

const fonts = [{
    value: '',
    name: '',
},{
    value: 'Roboto',
    name: 'Roboto',
    url: 'https://fonts.googleapis.com/css?family=Roboto'
},{
    value: 'Roboto Slab',
    name: 'Roboto Slab',
    url: 'https://fonts.googleapis.com/css?family=Roboto Slab'
},{
    value: 'Roboto Mono',
    name: 'Roboto Mono',
    url: 'https://fonts.googleapis.com/css?family=Roboto Mono'
},{
    value: 'Roboto Condensed',
    name: 'Roboto Condensed',
    url: 'https://fonts.googleapis.com/css?family=Roboto Condensed'
}]

const list = _.map(fonts, f => ({value: f.value, name: f.name }))
const urls = _(fonts).map('url').filter().value()

export default grapes.plugins.add('alternate-fonts', (editor) => {

    editor.StyleManager.addProperty('typography', {
        name: 'Alternate Fonts',
        property: 'font-family',
        type: 'select',
        defaults: '',
        list
    }, {at: 1})

    const config = editor.Canvas.getConfig().styles.push(...urls)
    // editor.Canvas.setConfig(config)

})
