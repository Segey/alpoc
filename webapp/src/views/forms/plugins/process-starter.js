import grapesjs from 'grapesjs'


const WRAPPER_CLASS = 'process-starter-wrapper'
const TITLE_CLASS = 'process-starter-title'
const NAME_CLASS = 'process-starter-name'
const BUTTON_CLASS = 'process-starter-button'

export default grapesjs.plugins.add('process-starter', (editor, options) => {

    // Script ---------------------------------------------------------------------------------------------------------
    const script = function (props) {
        if (typeof runCamundaProces === 'function') {
            runCamundaProces(props.process)
        }
    };

    // Component type -------------------------------------------------------------------------------------------------
    editor.Components.addType('process-starter', {
        model: {
            defaults: {
                script,
                process: {},
                traits: [{
                    name: 'process',
                    changeProp: true,
                    label: 'Process',

                    type: 'button',
                    text: 'Select',
                    full: true, // Full width button
                    command: (editor, trait) => {
                        options.processSelector()
                            .then(value => {
                                trait.set({value})
                            })
                    }
                }],
                content: `<button class="button is-primary ${BUTTON_CLASS}">СТАРТ</button>`,
                style: {
                    'display': 'block',
                },
                'script-props': ['process'],
            }
        }
    });

    // Component block ------------------------------------------------------------------------------------------------
    editor.Blocks.add('process-starter-block', {
        category: 'Appliner',
        label: 'Camunda Project Starter',
        attributes: {class: 'fa fa-play process-starter'},
        content: {type: 'process-starter'},
    });

    editor.TraitManager.addType('process-selector-type', {
        type: 'button',
        text: 'Select',
        full: true, // Full width button
        command: editor => {
            options.processSelector()
                .then( value => trait.set({value: value.process_id}) )
        },

    });

}
)