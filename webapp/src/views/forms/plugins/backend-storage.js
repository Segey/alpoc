import grapes from "grapesjs";

export default grapes.plugins.add('backend-storage', (editor, options) => {

    // GraphQL Trait --------------------------------------------------------------------------------------------------
    editor.StorageManager.add('backend-storage', {
        /**
         * Load the data
         * @param  {Array} keys Array containing values to load, eg, ['gjs-components', 'gjs-styles', ...]
         * @param  {Function} clb Callback function to call when the load is ended
         * @param  {Function} clbErr Callback function to call in case of errors
         */
        load(keys, clb, clbErr) {
            options.loadParts(options.form_key, keys)
                .then(parts =>clb(parts))
                .catch(error => clbErr(error))
        },

        /**
         * Store the data
         * @param  {Object} data Data object to store
         * @param  {Function} clb Callback function to call when the load is ended
         * @param  {Function} clbErr Callback function to call in case of errors
         */
        store(data, clb, clbErr) {
            options.saveParts(options.form_key, data)
                .then(() =>clb())
                .catch(error => clbErr(error))
        }
    });

}
)