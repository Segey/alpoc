import grapesjs from 'grapesjs'


function formByAction(action) {
    const components = []

    action.fields.forEach(field => {
        components.push(inputByType(field))
    })
    components.push(defaultsByAction(action))
    components.push(submitByAction(action))

    return components
}

function defaultsByAction(action) {
    // console.log('action', action)
    return [{
        tagName: 'input',
        attributes: {
            type: 'hidden',
            name: 'process-id',
            value: action.key,
        },
    },{
        tagName: 'input',
        attributes: {
            type: 'hidden',
            name: 'type',
            value: action.type
        },
    },{
        tagName: 'input',
        attributes: {
            type: 'hidden',
            name: 'subName',
            value: action.subName
        },
    },{
        tagName: 'input',
        attributes: {
            type: 'hidden',
            name: 'subType',
            value: action.subType
        },
    }]
}

function inputByType(field) {
    console.log('field',field)
    return {
        tagName: 'div',
        attributes: {class: 'field'},
        components: [{
            tagName: 'input',
            attributes: {
                name: field.key,
                placeholder: field.name,
                type: 'text',
                class: 'input',
                'apl-type': field.type,
            },
        }]
    }
}

function submitByAction(action) {
    // console.log('action',action)
    return {
        tagName: 'div',
        attributes: {class: 'field'},
        components: [{
            tagName: 'input',
            attributes: {
                name: 'value',
                type: 'submit',
                class: 'button is-link action-submit-button',
                value: action.subType === 'start_event' ? `Запусть '${action.name}'` : `Сохранить '${action.subName}'`,
            },
        }]
    }
}


export default grapesjs.plugins.add('action-caller', (editor, options) => {

        editor.Blocks.add('action-caller-form', {
            category: 'Appliner➡Actions',
            label: 'Action Block',
            attributes: {class: 'fa fa-hand-o-right'},
            content: {type: 'action-caller-form-component'},
        });
        editor.Components.addType('action-caller-form-component', {
            model: {
                defaults: {
                    tagName: 'form',
                    draggable: true, // Can be dropped only inside `form` elements
                    droppable: true, // Can't drop other elements inside
                    attributes: { // Default attributes
                        action: null,
                        class: "form",
                    },
                    traits: [{
                        type: 'button',
                        label: 'Действие:',
                        text: 'Выбрать',
                        full: true,
                        command: editor => {
                            const component = editor.getSelected();
                            options.actionSelector().then(action => {
                                let components = component.components();
                                component.empty()
                                components.add(formByAction(action))
                            })
                        },
                    },],
                    style: {
                        'display': 'block',
                        'justify-content': 'flex-start',
                        'align-items': 'stretch',
                        'flex-wrap': 'nowrap',
                        'padding': '10px',
                        width: '100%',
                        // background: 'red',
                    },
                }
            }
        });




        editor.Blocks.add('action-caller-block', {
            category: 'Appliner➡Actions',
            label: 'Call Action',
            attributes: {class: 'fa fa-bolt'},
            content: {type: 'action-caller-block-component'},
        });

        editor.Components.addType('action-caller-block-component', {
            model: {
                defaults: {
                    tagName: 'input',
                     draggable: '[data-gjs-type=action-caller-form-component], [data-gjs-type=action-caller-form-component] *', // Can be dropped only inside `form` elements
                    droppable: false, // Can't drop other elements inside
                    attributes: { // Default attributes
                        type: 'submit',
                        value: 'Run'
                    },
                }
            }
        });



    }
)