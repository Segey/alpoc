import grapesjs from 'grapesjs'
const { parse } = require('graphql');

export default grapesjs.plugins.add('graphql-collection', (editor, options) => {

    // Component type -------------------------------------------------------------------------------------------------
    editor.Components.addType('graph-ql-collection', {
        model: {
            defaults: {
                attributes: {
                    query: null,
                    path: null,
                    class: "box graphql-query-component",
                },
                traits: [{
                    type: 'button',
                    label: 'Запрос:',
                    text: 'Выбрать',
                    full: true,
                    command: editor => {
                        const component = editor.getSelected();
                        options.queryBuilder()
                            .then(query => {
                                component.setAttributes({
                                    path: query.data_path,
                                    query: query.graph_query,
                                })
                                const query_parsed = parse(query.graph_query);
                                const fields = query_parsed.definitions[0]
                                    .selectionSet.selections[0]
                                    .selectionSet.selections[0]
                                    .selectionSet.selections[0]
                                    .selectionSet.selections
                                    .map(({name}) => name.value)
                                const components = fields.map(field => ({
                                    tagName: 'div',
                                    attributes: {
                                    },
                                    content: `{{ ${field} }}`
                                }))
                                component.empty()
                                component.components().add(components)
                            })
                    },
                }],
                style: {
                    'display': 'block',
                    'justify-content': 'flex-start',
                    'align-items': 'stretch',
                    'flex-wrap': 'nowrap',
                    'padding': '10px',
                    // width: '100%',
                    // background: 'red',
                },
            }
        }
    });

    // Component block ------------------------------------------------------------------------------------------------
    editor.Blocks.add('graphql-collection-block', {
        category: 'Appliner➡Queries',
        label: 'Graph QL Collection',
        attributes: {class: 'fa fa-quora '},
        content: {type: 'graph-ql-collection'},
    });

})