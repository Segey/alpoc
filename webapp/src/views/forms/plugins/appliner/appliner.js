export default (editor, opts = {}) => {
  const plugin = editor.BlockManager.add; // Alias for convenience.

  plugin('appliner_templates_contact', {
    label: 'Contact form',
    content: `
    <section class="section">
      <div class="container py-4">
        <h2 class="title has-text-centered mb-6">User task</h2>
        <div class="columns">
          <div class="column is-12">
            <form>
              <div class="field">
                <div class="control">
                  <input class="input" name="fullname" type="text" placeholder="ФИО">
                </div>
              </div>
              <div class="field">
                <div class="control">
                  <input class="input" name="age" type="number" placeholder="Возраст">
                </div>
              </div>
              <div class="field">
                <div class="control">
                  <button class="button is-primary is-fullwidth" type="submit">Ок</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>`,
    category: 'Appliner➡️Templates',
    media:    '<img src="appliner.png" height=32 width=32 loading="lazy">'
  });



}
