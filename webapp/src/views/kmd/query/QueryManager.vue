<template>

    <div class="flex flex-row w-full h-full">

        <aside class="flex-none flex flex-col w-60 border-r">
            <a class="p-3 flex-none flex flex-row justify-between items-center cursor-pointer border-b">
                <div class="text-lg font-bold">Запросы</div>
                <icon-add @click="addQuery"/>
            </a>

            <div class="flex-grow flex flex-col">
                <template v-for="query in queries">
                    <div @click="onSelectQuery(query)"
                         class="p-2 flex flex-row center justify-between w-full h-16 border-b cursor-pointer hover:text-orange-500 group"
                         :class="{'bg-gray-200': selectedQuery && selectedQuery.id === query.id}">
                        <div>
                            <div>{{query.name}} <span v-if="query.description" class="text-sm text-gray-800 group-hover:text-orange-500">{{query.description}}</span> </div>
                            <div class="mt-2 text-xs text-gray-500 group-hover:text-orange-500">{{ query.created_at }}</div>
                        </div>
                        <div>
                            <icon-delete class="invisible group-hover:visible" @click="onQueryDelete(query)"/>
                        </div>
                    </div>
                </template>
            </div>
        </aside>

        <aside class="flex-grow">
            <router-view/>
        </aside>

        <a-dialog v-model="addQueryDialog" title="Создание запроса" @close="addQueryDialog = false">

            <div class="space-y-6 w-4/5">
                <div>
                    <div class="font-semibold text-sm">Имя запроса</div>
                    <div class="text-gray-500 text-sm mb-2">Название</div>
                    <input v-model="queryForm.name"
                           class="rounded border focus:outline-none focus:border-sky-500 focus:ring-1 focus:ring-sky-500 shadow p-2 w-full"/>
                </div>
                <div>
                    <div class="font-semibold text-sm">Код запроса</div>
                    <div class="text-gray-500 text-sm mb-2">Код латинскими буквами</div>
                    <input v-model="queryForm.key"
                           class="rounded border focus:outline-none focus:border-sky-500 focus:ring-1 focus:ring-sky-500 shadow p-2 w-full"/>
                </div>
                <div>
                    <div class="font-semibold text-sm">Описание<span class="ml-1 text-gray-500 text-xs">(Необязательно)</span></div>
                    <div class="text-gray-500 text-sm mb-2">Любой текст</div>
                    <input v-model="queryForm.description"
                           class="rounded border focus:outline-none focus:border-sky-500 focus:ring-1 focus:ring-sky-500 shadow p-2 w-full"/>
                </div>
                <div>
                    <div class="font-semibold text-sm">Модель данных</div>
                    <div class="flex justify-between p-2 border rounded">
                        <div class="">{{queryForm.model ? queryForm.model.name : null}}</div>
                        <button class=" cursor-pointer border hover:shadow" @click="onSelectModel">
                            <svg class="h-6 w-6" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                <rect x="1" y="4" width="22" height="16" rx="2" ry="2"/>
                                <line x1="1" y1="10" x2="23" y2="10"/>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>

            <template slot="actions">
                <button-submit :loading="loading" :disabled="!queryFormIsReady" @click="onQueryFormSubmit">Сохранить</button-submit>
            </template>

        </a-dialog>

        <a-dialog v-model="queryDeleteConfirmation" title="Подверждение" @close="queryDeleteConfirmation = false">
            <div>Удалить запрос?</div>

            <template slot="actions">
                <button-delete :loading="loading"  @click="onQueyDeleteSubmit">Удалить</button-delete>
            </template>
        </a-dialog>

        <a-dialog v-model="selectModelDialog" title="Выбор модели" @close="selectModelDialog = false">
            <div class="w-64">
                <label v-for="model in models" class="w truncate flex justify-between items-center">
                    {{model.name}}
                    <input name="select-model" :value="model.key" v-model="model_key" type="radio" class="form-radio m-2" />
                </label>
            </div>

            <template slot="actions">
                <button-submit :loading="loading" :disabled="!model_key" @click="onModelSelected">Выбрать</button-submit>
            </template>
        </a-dialog>

    </div>

</template>

<script>
import IconAdd from "@/components/IconAdd";
import ADialog from "@/components/ADialog";
import IconLoading from "@/components/IconLoading";

import IconDelete from "@/components/IconDelete";
import ButtonSubmit from "@/components/ButtonSubmit";
import ButtonDelete from "@/components/ButtonDelete";
import CodeMirrorEditor from "@/views/kmd/components/CodeMirrorEditor";

import api from '@/api'
import {slugify} from "transliteration";
import _ from "lodash";

export default {
    components: {CodeMirrorEditor, ButtonDelete, ButtonSubmit, IconDelete, ADialog, IconAdd, IconLoading},
    data: () => ({

        loading: false,

        queries: [],
        addQueryDialog: false,
        queryForm: {
            name: null,
            key: null,
            description: null,
            model: {},
        },

        model_key: null,
        selectModelDialog: false,
        models: [],

        selectedQuery:  {},

        selectedQueryToDelete: null,
        queryDeleteConfirmation: false,

        result: null,

        viewMode: 'json',
    }),

    mounted() {
        this.getAllQueries();
        this.getAllModels();
    },

    watch: {
        'queryForm.name'(t) {
            this.queryForm.key = slugify(t)
        },

    },

    computed: {
        queryFormIsReady() {
            return this.queryForm.name
        },
        'queryForm.name'(t) {
            this.queryForm.key = t && slugify(t)
        },
    },

    methods: {

        getAllQueries() {
            return api.kmd.readAllQueries()
            .then(queries => this.queries = queries)
            .then(queries => {
                if (!this.selectedQuery && queries && queries.length) {
                    this.selectedQuery = queries[0]
                    this.onSelectQuery(queries[0])
                }
            })
        },

        getAllModels() {
            api.kmd.getAll().then(models => this.models = models)
        },

        onSelectModel() {
            this.model_key = this.queryForm.model.key
            this.selectModelDialog = true
        },

        onModelSelected() {
            this.queryForm.model = _.find(this.models, {key: this.model_key})
            this.queryForm.model_key = this.queryForm.model.key
            this.selectModelDialog = false
        },

        addQuery() {
            this.queryForm.name = null;
            this.queryForm.key = null;
            this.queryForm.description = null;
            this.queryForm.model = {};
            this.addQueryDialog = true;
        },

        onQueryFormSubmit() {
            this.loading = true;
            api.kmd.createQuery(this.queryForm)
            .then(() => {
                this.addQueryDialog = false;
                return this.getAllQueries();
            })
            .finally(() => this.loading = false)
        },

        onSelectQuery(query) {
            if (query) {
                if (query.id !== this.selectedQuery.id) {
                    this.selectedQuery = Object.assign({}, query)
                    this.$router.push({name: 'query-editor', params: {queryKey: query.key}}).catch(()=>{})
                }
            }
            else
                this.$router.push({name: 'query-empty'})
        },

        onQueryDelete(query) {
            this.selectedQueryToDelete = Object.assign({}, this.selectedQueryToDelete, query);
            this.queryDeleteConfirmation = true;
        },

        onQueyDeleteSubmit() {
            this.loading = true;
            api.kmd.deleteQuery(this.selectedQueryToDelete)
                .then(() => {
                    this.queryDeleteConfirmation = false;
                    if (this.selectedQueryToDelete.id === this.selectedQuery.id)
                        this.selectedQuery = null
                    return this.getAllQueries();
                })
                .finally(() => this.loading = false)
        },

    }

}
</script>
