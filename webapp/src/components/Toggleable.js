export default {
    props: {
        value: {},
    },

    computed: {
        isActive: {
            get() {return this.value},
            set(v) {this.$emit('input', v)}
        }
    }
}
