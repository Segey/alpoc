

export default $axios => ({

    readAll() {
        return $axios.get("processes");
    },

    read(key) {
        return $axios.get(`processes/${key}`);
    },

    readContent(key) {
        return $axios.get(`processes/${key}/content`);
    },

    create(key, name) {
        return $axios.post("processes",  {key, name});
    },

    update( oldKey, {key, name} ) {
        return $axios.put(`processes/${oldKey}`, {key, name});
    },

    updateContent(  key, content) {
        const config = { headers: {'Content-Type': 'text/plain'} };
        return $axios.put(`processes/${key}/content`, content, config);
    },

    delete(key) {
        return $axios.delete(`processes/${key}`);
    },

    // -------------------------------------------------------

    loadActions() {
        return $axios.get(`actions`);
    },


});
