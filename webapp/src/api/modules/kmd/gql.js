import {getIntrospectionQuery} from "graphql";


export default $axios => ({

    //https://www.apollographql.com/blog/backend/schema-design/three-ways-to-represent-your-graphql-schema/
    getSchema() {
        const query = getIntrospectionQuery({})
        return $axios.post("", {query}).then(({data}) => data);
    },

    runQuery(query) {
        return $axios.post("", {query});
    }

});
