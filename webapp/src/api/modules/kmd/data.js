

export default $axios => ({

    select(mode_key) {
        return $axios.get(`/data/${mode_key}`);
    },

    create(mode_key, data) {
        return $axios.post(`/data/${mode_key}`, data);
    },

    update(mode_key, data_id, data) {
        return $axios.put(`/data/${mode_key}/${data_id}`, data);
    },

    delete(mode_key, data_id) {
        return $axios.delete(`/data/${mode_key}/${data_id}`);
    },

});
