

export default $axios => ({

    getAll() {
        return $axios.get("models");
    },

    getModel(model_key) {
        return $axios.get(`models/${model_key}`);
    },

    createModel(modelForm) {
        return $axios.post(`models`, modelForm);
    },

    deleteModel(model_key) {
        return $axios.delete(`models/${model_key}`);
    },

    // -------------------------------------------------------

    createField(model_key, field) {
        return $axios.post(`models/${model_key}`, field);
    },

    deleteField(model_key, field_key) {
        return $axios.delete(`models/${model_key}/${field_key}`);
    },

    // -------------------------------------------------------

    readAllQueries() {
        return $axios.get("queries");
    },

    createQuery(query) {
        return $axios.post("queries", query);
    },

    getQuery(query_key) {
        return $axios.get(`queries/${query_key}`);
    },

    updateQuery(query_key, query) {
        return $axios.put(`queries/${query_key}`, query);
    },

    deleteQuery(query_key) {
        return $axios.delete(`queries/${query_key}`);
    },

    // -------------------------------------------------------

    loadActions() {
        return $axios.get(`actions`);
    },


});
