

export default $axios => ({

    delete(id) {
        return $axios.delete(`form-inputs/${id}`);
    },

    create({page, name, type}) {
        return $axios.post(`form-inputs`,  {name, type, page});
    },

});
