

export default $axios => ({

    readByPage(page_id) {
        return $axios.get(`parts?page_eq=${page_id}`);
    },

    create(page, name, content) {
        return $axios.post(`parts`,  {page, name, content});
    },

    update(id, page, name, content) {
        return $axios.put(`parts/${id}`,  {page, name, content});
    },

});
