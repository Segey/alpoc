

export default $axios => ({

    readAll() {
        return $axios.get("processes");
    },

    read(id) {
        return $axios.get(`processes/${id}`);
    },

    create(key, title, project) {
        return $axios.post("processes",  {key, title, project});
    },

    update(id, key, title, content, project) {
        return $axios.put(`processes/${id}`,  {key, title, content, project});
    },

    delete(id) {
        return $axios.delete(`processes/${id}`);
    },


});
