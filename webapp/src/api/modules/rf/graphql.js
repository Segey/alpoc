

export default $axios => ({

    query(q) {
        return $axios.post("graphql",  {query: q});
    },

    /**
     *
     const data = `{ data: { title: "Author", part: $part, content: $content } }`
     const schema = `{ page { title part content } }`
     const mutation = `createPage(input: ${data}) ${schema}`
     const query = `mutation createPage($part: String, $content: String) {${mutation}}`
     const variables = {
        part: key,
        content: value,
     }
     * @param q
     * @param v
     * @returns {*}
     */
    mutate (q, v) {
        return $axios.post("graphql",  {query: q, variables: v});
    },

});
