

export default $axios => ({

    read(key) {
        return $axios.get(`pages/${key}`);
    },

    readAll(type) {
        const filter = type ? `?type=${type}` : ``
        return $axios.get(`pages/${filter}`);
    },

    create(key, name, description, type) {
        return $axios.post("pages",  {key, name, description, type});
    },

    update(key, name, description, type) {
        return $axios.put(`pages/${key}`, {name, description, type});
    },

    delete(key) {
        return $axios.delete(`pages/${key}`);
    },

    getParts(form_key) {
        return $axios.get(`pages/${form_key}/parts`);
    },

    saveParts(form_key, parts) {
        return $axios.put(`pages/${form_key}/parts`, parts);
    },

});
