import axios from './axios'


const axiosKp = axios(process.env.VUE_APP_KP_URL);
const axios_rf = axios(process.env.VUE_APP_RF_URL);

const axiosBld = axios(process.env.VUE_APP_KP_URL);

const axiosKmd = axios(process.env.VUE_APP_KMD_URL);

const axiosData = axios(process.env.DATA_URL || 'http://localhost:8093/');
const axiosGraphile = axios(process.env.VUE_APP_GRAPHILE_URL);


import processes from "./modules/kp/processes";
import pages from "./modules/rf/pages";
import builder from "./modules/kp/builder";

import kmd from "./modules/kmd/kmd";
import data from "./modules/kmd/data";
import gql from "./modules/kmd/gql";


const repositories = {

    processes: processes(axiosKp),
    pages: pages(axios_rf),

    kmd: kmd(axiosKmd),
    data: data(axiosData),
    gql: gql(axiosGraphile),

    builder: builder(axiosBld),
}


export default repositories;
