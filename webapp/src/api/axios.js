import axios from 'axios'


export default baseURL => {

    const axios_inst = axios.create({baseURL})

    axios_inst.interceptors.request.use((config) => {
        return config;
    });

    axios_inst.interceptors.response.use(
        response => {
            return Promise.resolve(response.data)
        },
        (error) => {
            if (error.response )
                console.log('error', error.response)
            else
                console.log('error', error.toString())
            const data = error.response ? error.response.data ? error.response.data : error.response : error
            return Promise.reject(data);
        }
    );

    return axios_inst;

};
