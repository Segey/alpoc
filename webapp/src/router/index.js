import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)



const routes = [{
    path: '/',
    name: 'main',
    component: () => import('../views/Main'),
    children: [{
        path: "builder",
        name: 'builder',
        props: (route) => ({ type: route.query.type }),
        component: () => import('@/views/Builder'),
    },{
        path: "data",
        name: 'data-model',
        component: () => import('@/views/kmd/Main'),
        children: [{
            path: "model",
            name: 'data-model-manager',
            // props: (route) => ({ type: route.query.type }),
            component: () => import('@/views/kmd/model/DataModelManager'),
            children: [{
                path: ":modelKey",
                name: 'data-model-editor',
                props: true,
                component: () => import('@/views/kmd/model/DataModelEditor'),
            }],
        },{
            path: "query",
            name: 'query-manager',
            component: () => import('@/views/kmd/query/QueryManager'),
            children: [{
                path: "",
                name: 'query-empty',
                component: () => import('@/views/kmd/query/QueryEmpty'),
            },{
                path: ":queryKey",
                name: 'query-editor',
                props: true,
                component: () => import('@/views/kmd/query/QueryEditor'),
            }]
        },{
            path: "data",
            name: 'data-manager',
            component: () => import('@/views/kmd/data/DataManager'),
            children: [{
                path: ":modelKey",
                name: 'data-editor',
                props: true,
                component: () => import('@/views/kmd/data/DataEditor'),
            }]
        }]

    },{
        path: "forms",
        name: 'forms',
        component: () => import('@/views/forms/FormManager'),
        children: [{
            path: ":formKey",
            name: 'form-editor',
            props: true,
            component: () => import('@/views/forms/FormEditor'),
        }]
    },{
        path: "processes",
        name: 'processes',
        component: () => import('@/views/bpmn/ProcessManager'),
        children: [{
            path: ":processKey",
            name: 'process-editor',
            props: true,
            component: () => import('@/views/bpmn/ProcessEditor'),
        }]
    }]
}]

const router = new VueRouter({
    mode: 'history',
    base: process.env.VUE_APP_BASE_URL,
    routes
})

export default router
