# Run KP
cd backend-kp
./mvnw spring-boot:run -Dspring-boot.run.profiles=local &

# Run KMD
cd ../backend-kmd
pm2 --name backend-kmd start npm -- run

# Run KMD
cd ../backend-rf
pm2 --name backend-rf start npm -- run

# Run Sanbox
cd ../sandbox
pm2 --name sandbox start npm -- run

cd ../webapp
#npm run serve
pm2 --name sandbox2 start npm -- run serve
