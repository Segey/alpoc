const fs = require('fs');
const path = require('path');
const process = require('process');

const ejs = require('ejs');

const {
    Liquibase,
    POSTGRESQL_DEFAULT_CONFIG,
} = require('liquibase');


function runChangeSet(changeLogFile) {

    const myConfig = {
        ...POSTGRESQL_DEFAULT_CONFIG,
        changeLogFile: changeLogFile,
        classpath: 'lib/postgresql-42.2.24.jre6.jar:lib/snakeyaml-1.12.jar',
        url: `jdbc:postgresql://${process.env.DATABASE_HOST}:${process.env.DATABASE_PORT}/${process.env.DATABASE_NAME}`,
        username: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
    };

    const instance = new Liquibase(myConfig);
    return instance.update()
}

function runTemplate(id, data, template_name) {

    const root = process.cwd();
    const templates = 'templates';
    const target = 'staging';
    const changeset_file = `${id}.yaml`;

    const template_path = path.join(root, templates, template_name)
    const target_path = path.join(root, target, changeset_file)

    ejs.renderFile(template_path, data, {}, (err, str) => { fs.writeFileSync(target_path, str) });

    const changeLogFile = path.join(target, changeset_file)

    return runChangeSet(changeLogFile)
        .catch(err => {
            // fs.unlinkSync(target_path);
            let message = "Unexpected error"
            if (err.message) {
                const mtch = err.message.match(/.*Unexpected error running Liquibase(.*)/g)
                message = mtch[0]
            }
            throw {message}
        })
}

module.exports = {runChangeSet, runTemplate}