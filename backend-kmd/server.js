// load the things we need
require('dotenv').config()
var express = require('express');
var cors = require('cors')

const {runChangeSet} = require('./liquibase-utils.js')
const process = require("process");

const { postgraphile } = require("postgraphile");


async function run() {

    await runChangeSet('db_initialize.yaml')

    var app = express();
    // set the view engine to ejs
    // app.set('view engine', 'ejs');
    app.use(cors())
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    app.use('/models', require('./routes/model'))
    app.use('/data', require('./routes/data'))
    app.use('/queries', require('./routes/query'))
    app.use('/actions', require('./routes/actions'))

    app.use(
        // 'graphql',
        postgraphile(
            {
                database:  process.env.DATABASE_NAME,
                user:      process.env.DATABASE_USERNAME,
                password:  process.env.DATABASE_PASSWORD,
                host:      process.env.DATABASE_HOST,
                port:      process.env.DATABASE_PORT,
            },
            "product",
            {
                watchPg: true,
                graphiql: true,
                enhanceGraphiql: true,
            }
        )
    );

    app.listen(process.env.PORT, process.env.HOST);
    console.log(`${process.env.PORT} is opened`);

}

run().catch(error => {
    console.error("Fatal error", error)
})