var express = require('express');
var router = express.Router();

_ = require('lodash')

knex = require('./knex')

const schema = 'apl_kmd'


function createName(key) {
    let node = `all ${key}`
    if (!node.endsWith('s')) node += 's'
    node = _.camelCase(node);
    return node
}

function createGraphQLQuery(fields, {key}) {
    const node = createName(key)
    const field_list = ['id', 'created_at', 'updated_at', ...fields].map(f => _.camelCase(f)).join(' ')
    return `query { ${node} { edges { node { ${field_list} } } } }`
}

function createGraphQLDataPath(fields, {key}) {
    const node = createName(key)
    return `data.${node}.edges`
}

async function getAllQueries() {
    return await knex.withSchema(schema).select().from(`query_catalog`)
}

async function getQuery(query_key) {
    const query = await knex.withSchema(schema).select().from(`query_catalog`).where({key: query_key}).first()
    query.fields = await knex.withSchema(schema).select().from(`query_fields_catalog`).where({query_id: query.id})
    query.model = await knex.withSchema(schema).select().from(`model_catalog`).where({id: query.model_id}).first()
    query.model.fields = await knex.withSchema(schema).select().from(`model_column_catalog`).where({model_id: query.model_id})
    return query
}

async function createQuery(query_data) {

    const {key, name, model_key, description, is_single, use_pagination, graph_query, data_path, form} = query_data;

    const model = await knex.withSchema(schema).select("id").from(`model_catalog`).where({key: model_key}).first();

    await knex.withSchema(schema).table('query_catalog').insert({
        key,
        name,
        description,
        model_id: model.id,
        is_single,
        use_pagination,
        graph_query,
        data_path,
        form,
    })
}

async function updateQuery(query_key, query_data) {

    const {key, name, model_key, description, is_single, use_pagination, form, field_keys} = query_data;

    const query = await knex.withSchema(schema)
        .select().from(`query_catalog`).where({key: query_key}).first()

    const model = await knex.withSchema(schema)
        .select().from(`model_catalog`).where({key: model_key}).first()

    const model_fields = await knex.withSchema(schema)
        .select().from(`model_column_catalog`).where({model_id: model.id})


    const fields_by_key = _.keyBy(model_fields, 'key')
    const fields = _.map(field_keys, field => {
        return {
            key: fields_by_key[field].key,
            name: fields_by_key[field].name,
            description: fields_by_key[field].description,
            column_id: fields_by_key[field].id,
            query_id: query.id,
        }
    })

    const graph_query = createGraphQLQuery(field_keys, model)
    const data_path = createGraphQLDataPath(field_keys, model)

    await knex.withSchema(schema).table('query_catalog').update({
        key,
        name,
        description,
        model_id: model.id,
        is_single,
        use_pagination,
        graph_query,
        data_path,
        form,
        updated_at: knex.raw('CURRENT_TIMESTAMP'),
    }).where({key: query_key})


    await knex.withSchema(schema).table('query_fields_catalog').delete().where({query_id: query.id})
    if (fields && fields.length)
        await knex.withSchema(schema).table('query_fields_catalog').insert(fields)


}

async function removeQuery(query_key) {
    const query = await knex.withSchema(schema)
        .select().from(`query_catalog`).where({key: query_key}).first()

    await knex.withSchema(schema).table('query_fields_catalog').delete().where({query_id: query.id})
    await knex.withSchema(schema).table(`query_catalog`).delete().where({key: query_key})

}

//----------------------------------------------------------------------------------------------------------------------


/**
 * Получить все запросы
 */
router.get('', async (req, res) => {

    try {
        res.status(200).send(await getAllQueries())
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})

/**
 * Создать запрос
 */
router.post('', async (req, res) => {

    const {key, name, model_key, description, is_single, use_pagination, graph_query, data_path, form} = req.body;

    try {
        await createQuery({key, name, model_key, description, is_single, use_pagination, graph_query, data_path, form})
        res.status(200).send(await getAllQueries())

    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
});

/**
 * Получить запрос
 */
router.get('/:query_key', async (req, res) => {

    const query_key = req.params.query_key
    try {
        res.status(200).send(await getQuery(query_key))

    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }

});

/**
 * Обновить запрос
 */
router.put('/:query_key', async (req, res) => {

    const query_key = req.params.query_key
    const {key, name, model_key, description, is_single, use_pagination, graph_query, data_path, form, field_keys} = req.body;

    try {
        await updateQuery(query_key, {key, name, model_key, description, is_single, use_pagination, graph_query, data_path, form, field_keys})
        res.status(200).send(await getQuery(query_key))

    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }

});

/**
 * Удлаить модель
 */
router.delete('/:query_key', async (req, res) => {

    const query_key = req.params.query_key

    try {
        await removeQuery(query_key)
        res.status(200).send(await getAllQueries())

    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }

});

//--------------------------------------------------------------------------------------------------------------------

module.exports = router;
