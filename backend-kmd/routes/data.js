const express = require('express');
const _ = require("lodash");
const router = express.Router();
const uniqid = require('uniqid')

const knex = require('./knex')
const schema = 'product'


/**
 * Получить все записи
 */
router.get('/:model_id', async (req, res) => {
    const table_name = req.params.model_id
    try {
        const rows = await knex.withSchema(schema).select().from(table_name)
        res.json(rows)

    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})

/**
 * Создать запись
 */
router.post('/:model_id', async (req, res) => {

    try {
        const table_name = req.params.model_id
        const data = req.body;

        const fields = {
            id: uniqid(),
            created_at: knex.raw('CURRENT_TIMESTAMP'),
            updated_at: knex.raw('CURRENT_TIMESTAMP'),
        }

        _(data).forEach((value, field) => {
            if ( ['id', 'created_at', 'updated_at'].includes(field)) return;
            if (_.isNil(value))
                fields[field] = null
            else
                fields[field] = value
        })

        await knex.withSchema(schema).table(table_name).insert(fields)
        const rows = await knex.withSchema(schema).select().from(table_name)
        res.json(rows)

    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }

});

/**
 * Обновить запись
 */
router.put('/:model_id/:rec_id', async (req, res) => {

    try {
        const table_name = req.params.model_id
        const rec_id = req.params.rec_id
        const data = req.body;

        const fields = {
            updated_at: knex.raw('CURRENT_TIMESTAMP'),
        }

        _(data).forEach((value, field) => {
            if ( ['id', 'created_at', 'updated_at'].includes(field)) return;
            fields[field] = value
        })

        await knex.withSchema(schema).table(table_name).update(fields).where({id: rec_id})
        const rows = await knex.withSchema(schema).select().from(table_name)
        res.json(rows)

    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }

});

/**
 * Удлаить модель
 */
router.delete('/:model_id/:rec_id', async (req, res) => {

    try {
        const table_name = req.params.model_id
        const rec_id = req.params.rec_id

        await knex.withSchema(schema).table(table_name).delete().where({id: rec_id})
        const rows = await knex.withSchema(schema).select().from(table_name)
        res.json(rows)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }

});


module.exports = router;
