var express = require('express');
var router = express.Router();
const _ = require('lodash')

const knex = require('./knex')

const schema = 'apl_kmd'


/**
 * Получить все действия
 */
router.get('', async (req, res) => {
    try {

        const actions = []

        const models = await knex.withSchema(schema).select().table('model_catalog')
        for (const model of models) {

            const allFields = await knex.withSchema(schema).select().table('model_column_catalog').where({model_id: model.id})
            const fields = _(allFields).filter('user_field').map(field => ({
                key: field.key,
                name: field.name,
                type: field.apl_type,
            }))

            const deleteModel = {
                type: "kmd",
                key: model['key'],
                name: model['name'],
                subKey: 'delete',
                subType: 'delete',
                subName: "Удаление записи",
                fields: []
            }

            const insertModel = {
                type: "kmd",
                key: model['key'],
                name: model['name'],
                subKey: 'insert',
                subType: 'insert',
                subName: "Добавление записи",
                fields,
            }

            const updateModel = {
                type: "kmd",
                key: model['key'],
                name: model['name'],
                subKey: 'update',
                subType: 'update',
                subName: "Обновление записи",
                fields,
            }

            actions.push(deleteModel, insertModel, updateModel)
        }

        res.json(actions)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})

module.exports = router;
