var express = require('express');
var router = express.Router();

const uniqid = require('uniqid')
const escape = require('pg-escape')


const {runTemplate} = require('../liquibase-utils.js')

knex = require('./knex')

const schema = 'apl_kmd'

const apl_types = {
    'single-line' : 'varchar(128)',
    'multy-line' : 'varchar(1024)',
    'rich-text' : 'varchar',
    'integer' : 'integer',
    'float' : 'real ',
    'timestamp' : 'timestamp ',
}

const escapeText = (str) => escape.string(str ? str
        .replace(/[\\]/g, '\\\\')
        .replace(/["]/g, '\\\"')
        .replace(/[\/]/g, '\\/')
        .replace(/[\b]/g, '\\b')
        .replace(/[\f]/g, '\\f')
        .replace(/[\n]/g, '\\n')
        .replace(/[\r]/g, '\\r')
        .replace(/[\t]/g, '\\t')
    : str
)

const escapeIdent = (str) => escape.ident(str.replace(/\W/g, ''))
const escapeType = (str) => str.replace(/^\dA-Za-z-_/g, '')

/**
 * Получить все модели
 */
router.get('', async (req, res) => {
    try {
        const models = await knex.withSchema(schema).select().table('model_catalog')
        res.status(200).send(models)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})

/**
 * Получить модель
 */
router.get('/:model_key', async (req, res) => {

    const table_key = req.params.model_key

    try {
        const model = await knex.withSchema(schema).select().table('model_catalog').where({key: table_key}).first()
        if (model)
            model.fields = await knex.withSchema(schema).select().table('model_column_catalog')
                .whereIn('model_id', function () {
                    return this.withSchema(schema).select('id').from('model_catalog').where({key: table_key})
                })
        res.status(200).send(model)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }

});

/**
 * Создать модель
 */
router.post('', async (req, res) => {

    const key = escapeIdent(req.body.key);
    const name = escapeText(req.body.name);
    const description = escapeText(req.body.description);

    const uid = uniqid();
    const id = `${uid}_create_table_${key}`;
    const data = {id, table_name: key, name, description}

    const template_name = 'create_table.yaml';

    runTemplate(id, data, template_name).then(() => {
        res.sendStatus(200)
    }).catch(error => {
        console.error(error)
        res.status(500).send(error)
    })
});

/**
 * Удлаить модель
 */
router.delete('/:model_key', async (req, res) => {

    const table_name = escapeIdent(req.params.model_key);

    const uid = uniqid();
    const id = `${uid}_drop_table_${table_name}`;
    const data = {id, table_name}

    const template_name = 'drop_table.yaml';

    runTemplate(id, data, template_name).then(() => {
        res.sendStatus(200)
    }).catch(error => {
        console.error(error)
        res.status(500).send(error)
    })
});

//--------------------------------------------------------------------------------------------------------------------

/**
 * Получить все столобцы модели
 */
router.get('/:model_key', async (req, res) => {

    const table_name = escapeIdent(req.params.model_key);

    try {
        const fields = await knex.withSchema(schema).select().table('model_column_catalog')
            .whereIn('model_key', function () {
                return this.withSchema(schema).select('id').from('model_catalog').where({key: table_name})
            })
        res.status(200).send(fields)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }

});

/**
 * Добавление столбца
 */
router.post('/:model_key', async (req, res) => {

    const table_name = escapeIdent(req.params.model_key);

    const key = escapeIdent(req.body.key);
    const name = escapeText(req.body.name);
    const description = escapeText(req.body.description);
    const is_unique = !!req.body.is_unique;
    const apl_type = escapeType(req.body.type);
    const type = apl_types[apl_type]

    const uid = uniqid();
    const id = `${uid}_add_column_${table_name}`;
    const data = {id, table_name, column_name: key, name, description, apl_type, type, is_unique}

    const template_name = 'add_column.yaml';

    runTemplate(id, data, template_name).then(() => {
        res.sendStatus(200)
    }).catch(error => {
        console.error(error)
        res.status(500).send(error)
    })

})

/**
 * Удалние столбца
 */
router.delete('/:model_key/:column_key', async (req, res) => {

    const table_name = escapeIdent(req.params.model_key)
    const column_name = escapeIdent(req.params.column_key)

    const uid = uniqid();
    const id = `${uid}_drop_column_${table_name}`;
    const data = {id, table_name, column_name}

    const template_name = 'drop_column.yaml';

    runTemplate(id, data, template_name).then(() => {
        res.sendStatus(200)
    }).catch(error => {
        console.error(error)
        res.status(500).send(error)
    })

})

module.exports = router;
