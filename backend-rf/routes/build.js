const express = require('express');
const router = express.Router();

const grapesjs = require('grapesjs')

knex = require('../controllers/knex')

const schema = 'public'

const PART = {
    gjs_css:         'gjs-css',
    gjs_assets:      'gjs-assets',
    gjs_styles:      'gjs-styles',
    gjs_components:  'gjs-components',
    gjs_html:        'gjs-html',
}

async function getPart(page_id, part_cd) {
    return await knex.withSchema(schema).select().table('parts').where({page: page_id, name: part_cd}).first()
}


/** Собрать страницу по компоненту
 */
router.get('/:form_key', async (req, res) => {

    const editor = grapesjs.init({ headless: true, protectedCss: '' });
    try {
        const form_key = req.params.form_key
        const page = await knex.withSchema(schema).select().table('pages').where({key: form_key}).first()
        const components = await getPart(page.id, PART.gjs_components)


        const content = JSON.parse(components.content)

        const script = function (props) {
    if (typeof queryGraphQL === 'function') {
        const query = props.prop_query;
        console.log('query', query);

        queryGraphQL(query)

    }

        };
        editor.Components.addType('graph-ql-collection', {
            model: {
                defaults: {
                    script,
                    prop_query: "",
                    traits: [{
                        type: 'graphql-query',
                        name: 'prop_query',
                        changeProp: true,
                        label: 'GraphQL Query',
                    }],
                    style: {
                        'display': 'block',
                        'justify-content': 'flex-start',
                        'align-items': 'stretch',
                        'flex-wrap': 'nowrap',
                        'padding': '10px',
                        // width: '100%',
                        // background: 'red',
                    },
                    'script-props': ['prop_query'],
                }
            }
        });

        editor.addComponents(content);

        const html = editor.getHtml();

        res.send(html)
    } finally {
        editor.destroy();
    }
})


module.exports = router;
