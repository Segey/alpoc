const express = require('express');
const router = express.Router();

const _ = require('lodash')

const {read_all_pages, create_page, update_page, delete_page, read_page} = require('../controllers/pages');
const {save_part, save_parts} = require('../controllers/parts');

// -------------------------------------------------------------------------------------------

/**
 * Получить все страницы
 */
router.get('', async (req, res) => {
    try {
        const type = req.query.type
        const pages = await read_all_pages(type)
        res.json(pages)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})

/**
 * Создать форму
 */
router.post('', async (req, res) => {
    try {
        await create_page(req.body)
        const pages = await read_all_pages()
        res.json(pages)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})

/**
 * Получить форму
 */
router.get('/:form_key', async (req, res) => {
    try {
        const form_key = req.params.form_key
        const page = await read_page(form_key);
        res.json(page)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})

/**
 * Обновить форму
 */
router.put('/:form_key', async (req, res) => {
    try {
        const form_key = req.params.form_key
        const body = req.body
        await update_page(form_key, body)
        const page = await read_page(form_key);
        res.json(page)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})

/**
 * Удалить форму
 */
router.delete('/:form_key', async (req, res) => {
    try {
        const form_key = req.params.form_key
        await delete_page(form_key)
        const pages = await read_all_pages()
        res.json(pages)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})


// ------------------------------------------------------------------

/**
 * Save part
 */
router.put('/:form_key/part/:part_key', async (req, res) => {
    try {
        const form_key = req.params.form_key
        const part_key = req.params.part_key
        const content = req.body

        await save_part(form_key, part_key, content)

        const respage = await read_page(form_key);
        res.json(respage)

    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})


/**
 * Save parts
 */
router.put('/:form_key/parts', async (req, res) => {
    try {
        const form_key = req.params.form_key
        const parts = req.body

        await save_parts(form_key, parts)

        const respage = await read_page(form_key);
        res.json(respage)

    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})

/**
 * Get parts
 */
router.get('/:form_key/parts', async (req, res) => {
    try {
        const form_key = req.params.form_key
        const page = await read_page(form_key);
        const parts = _(page.parts).keyBy('key').mapValues('content')
        res.json(parts)

    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})


module.exports = router;
