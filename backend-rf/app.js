// load the things we need
require('dotenv').config()
var express = require('express');
var cors = require('cors')

async function run() {

    const app = express();

    app.set('view engine', 'ejs');
    app.use('/static', express.static('static'));

    app.use(cors())
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    app.use('/debug', require('./routes/debug'))
    app.use('/pages', require('./routes/pages'))

    app.listen(process.env.PORT, process.env.HOST);
    console.log(`${process.env.HOST}:${process.env.PORT} is opened`);

}

run().catch(error => {
    console.error("Fatal error", error)
})