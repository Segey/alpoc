knex = require('./knex')

const schema = 'apl_rf'



async function read_all_pages(type) {
    const condition = type ? {type} : {}
    const pages = await knex.withSchema(schema).select().table('pages').where(condition).orderBy(['name'])
    for (const page of pages) {
        page.fields = await knex.withSchema(schema).select().table('fields').where({page_id: page.id})
    }
    return pages
}

async function read_page(form_key) {
    const page = await knex.withSchema(schema).select().table('pages').where({key: form_key}).first()
    page.parts = await knex.withSchema(schema).select().table('parts').where({page_id: page.id})
    page.fields = await knex.withSchema(schema).select().table('fields').where({page_id: page.id})
    return page;
}

async function create_page({key, name, description, type}) {
    await knex.withSchema(schema).table('pages').insert({key, name, description, type})
    return await knex.withSchema(schema).select().table('pages').orderBy(['name'])
}

async function update_page(form_key, {name, description, type}) {
    const updated_at = knex.raw('CURRENT_TIMESTAMP')
    await knex.withSchema(schema).table('pages').update({name, description, type, updated_at}).where({key: form_key})
}

async function delete_page(form_key) {
    const page = await knex.withSchema(schema).table('pages').select('id').where({key: form_key}).first()
    await knex.withSchema(schema).table('parts').delete().where({page_id: page.id})
    await knex.withSchema(schema).table('fields').delete().where({page_id: page.id})
    await knex.withSchema(schema).table('pages').delete().where(page)
}



module.exports = {read_all_pages, read_page, create_page, update_page, delete_page};
