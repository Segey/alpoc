
knex = require('./knex')

const schema = 'apl_rf'


async function save_part(form_key, part_key, content) {

    const page = await knex.withSchema(schema).table('pages').select('id').where({key: form_key}).first()
    const parts = await knex.withSchema(schema).table('parts').select('id').where({page_id: page.id, key: part_key})

    if (parts.length)
        await knex.withSchema(schema).table('parts').update({content}).where(parts[0])
    else
        await knex.withSchema(schema).table('parts').insert({page_id: page.id, key: part_key, content})

    const updated_at = knex.raw('CURRENT_TIMESTAMP')
    await knex.withSchema(schema).table('pages').update({updated_at}).where({key: form_key})

}

async function save_parts(form_key, parts) {
    const page = await knex.withSchema(schema).table('pages').select('id').where({key: form_key}).first()

    await knex.withSchema(schema).table('parts').delete().where({page_id: page.id})

    const keys = Object.keys(parts)
    for (const key of keys) {
        const content = parts[key]
        await knex.withSchema(schema).table('parts').insert({page_id: page.id, key, content})
    }

    const updated_at = knex.raw('CURRENT_TIMESTAMP')
    await knex.withSchema(schema).table('pages').update({updated_at}).where({key: form_key})
}


// -------------------------------------------------------------------------------------------


module.exports = {save_part, save_parts};
