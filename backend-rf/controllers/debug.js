
knex = require('./knex')

const schema = 'public'

const PART = {
    gjs_css:         'gjs-css',
    gjs_assets:      'gjs-assets',
    gjs_styles:      'gjs-styles',
    gjs_components:  'gjs-components',
    gjs_html:        'gjs-html',
}

async function get_page_preview(form_key) {
    const page = await knex.withSchema(schema).select().table('pages').where({key: form_key}).first()
    const html = await getPart(page.id, PART.gjs_html)
    const css = await getPart(page.id, PART.gjs_css)
    return {title: page.title, css: css.content, html: html.content}
}

module.exports = {get_page_preview};
