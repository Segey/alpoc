const process = require("process");
const pg_config = {
    host:      process.env.DATABASE_HOST,
    port:      process.env.DATABASE_PORT,
    database:  process.env.DATABASE_NAME,
    user:      process.env.DATABASE_USERNAME,
    password:  process.env.DATABASE_PASSWORD,
}

const knex = require('knex')({
    client: 'pg',
    connection: pg_config,
    pool: { min: 1, max: 5 }
});

module.exports = knex;
