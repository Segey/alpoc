

axios_process = axios.create({
    baseURL: `http://camunda.local:8084/engine-rest/`,
})

function startProcess({process_id, process_name}, props) {

    axios_process.post(`/process-definition/${process_id}/start`, props)
        .then(() => {
            notify_success({title:'Успех', message: `Процесс "${process_name}" успешно запущен`})
        })
        .catch(error => {
            notify_error({title:'Ошибка', message: error.message})
        })

}

function runCamundaProces({process_id, process_name}) {

    const props = {
        variables: {},
        businessKey : null
    }

    const elements = el.getElementsByClassName('process-starter-button');

    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', () => {
            startProcess({process_id, process_name}, props)
        });
    }

}

