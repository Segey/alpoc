// noinspection JSUnusedGlobalSymbols

/**
 *
 * @param template Шаблон, который будем заполнять данными
 * @param graphql_data Результат запроса GraphQL: { collection_type1: [array_of_type1], ... }
 * @returns {string}
 */
function render_graphql(template, graphql_data) {
    let rendered = ""
    console.log('render', graphql_data)
    graphql_data.forEach(view => {
        rendered += Mustache.render(template, view);
    })
    return rendered
}

/**
 *
 * @param container_el Элемент DOM, контейнер с шаблоном, который будем заполнять данными
 * @param graphql_data Результат запроса GraphQL: { collection_type1: [array_of_type1], ... }
 */
function render_container(container_el, graphql_data) {
    const template = container_el.innerHTML
    container_el.innerHTML = render_graphql(template, graphql_data)
}

/**
 * Запрос к серверу GraphQL
 * @param query
 * @param path
 * @returns {Promise<Response>}
 */
function queryGraphQL({query, path}) {

    const get_path = (obj, path) => path
        .split(".")
        .reduce( (res, key) => (res !== null && res !== undefined ? res[key] : res), obj)

    return fetch('http://localhost:8093/graphql', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({query}),
    })
        .then(res => {
            if (res.ok) return res.json();
            else        return res.json().then(text => { throw text })
        })
        .then( json => {
            const graphql_data =  get_path(json, path).map(({node}) => node)
            render_container(el, graphql_data);
        })
        .catch(errors => {
            console.log('errors', errors)
            notify(errors)
        })
}

// ------------------------------------------------------------------------------------------


