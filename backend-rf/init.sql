create schema if not exists apl_rf;

drop table apl_rf.parts;
drop table apl_rf.fields;
drop table apl_rf.pages;

create table apl_rf.pages(
    id              SERIAL PRIMARY KEY,
    key             VARCHAR NOT NULL UNIQUE,
    name            VARCHAR NOT NULL,
    description     varchar,
    created_at      timestamp default CURRENT_TIMESTAMP,
    updated_at      timestamp default CURRENT_TIMESTAMP,
    type            varchar NOT NULL
);

create table apl_rf.parts(
    id              SERIAL PRIMARY KEY,
    key             VARCHAR NOT NULL,
    content         VARCHAR NOT NULL,
    page_id         integer REFERENCES apl_rf.pages(id)
);

create table apl_rf.fields(
     id              SERIAL PRIMARY KEY,
     key             VARCHAR NOT NULL,
     name            VARCHAR NOT NULL,
     apl_type        VARCHAR NOT NULL,
     page_id         integer REFERENCES apl_rf.pages(id)
);

