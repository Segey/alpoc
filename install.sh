sudo apt install npm -y
sudo apt install default-jre -y
sudo snap install liquibase
sudo npm install pm2 -g

# Install KMD dependencies
cd backend-kmd
npm install

# Install RF dependencies
cd ../backend-rf
npm install

# Install Sandbox dependencies
cd ../sandbox
npm install

# Install WebApp dependencies
cd ../webapp
npm install

# Build KP
cd ../backend-kp
./mvnw spring-boot:run -Dspring-boot.run.profiles=local
