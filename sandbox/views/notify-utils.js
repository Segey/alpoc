const notify_success = window.createNotification({
    closeOnClick: false,
    displayCloseButton: true,
    positionClass: 'nfc-top-right',
    onclick: false,
    showDuration: 3500,
    theme: 'success'
})
const notify_error = window.createNotification({
    closeOnClick: false,
    displayCloseButton: true,
    positionClass: 'nfc-top-right',
    onclick: false,
    showDuration: 3500,
    theme: 'error'
})

function success(message) {
    notify_success({message})
}
function notify(errors) {
    if (errors.errors && Array.isArray(errors.errors))
        errors.errors.forEach(error => {
            notify_error({title:'Ошибка', message: error.message})
        })
    else
        notify_error({title:'Ошибка', message: errors.message})

}