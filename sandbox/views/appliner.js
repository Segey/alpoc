
const ACTION_SUBMIT_BUTTON_LOADING_CLASS = 'is-loading';
const ACTION_SUBMIT_BUTTON_SELECTOR = 'input.action-submit-button';
const axios_backend = axios.create({
    baseURL: `/api/`,
});


(function () {

    var items = document.querySelectorAll(ACTION_SUBMIT_BUTTON_SELECTOR);
    for (var i = 0, len = items.length; i < len; i++) {
        const handler = function() {

            this.classList.add(ACTION_SUBMIT_BUTTON_LOADING_CLASS);

            const data = {}
            const form = this.closest('form');
            const inputs = form.querySelectorAll("input, textarea");
            for (var j = 0, len = inputs.length; j < len; j++) {
                const input = inputs[j]
                data[input.name] = {
                    value: input.value,
                    apltype: input.getAttribute('apl-type'),
                }
            }

            axios_backend.post('action', data)
                .then(() => success(`Процесс успешно запущен`))
                .catch(notify)
                .finally(() => { this.classList.remove(ACTION_SUBMIT_BUTTON_LOADING_CLASS); })

            return false;
        }
        handler.bind(items[i])
        items[i].onclick = handler
    }

})();

const GRAPHQL_QUERY_COMPONENT_SELECTOR = '.graphql-query-component';

(function () {

    var items = document.querySelectorAll(GRAPHQL_QUERY_COMPONENT_SELECTOR);
    for (var i = 0, len = items.length; i < len; i++) {

        const element = items[i];
        const query = element.getAttribute('query')
        const path = element.getAttribute('path')
        queryGraphQL(element, {query,path})

    }

})();
