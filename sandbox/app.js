// load the things we need
require('dotenv').config()
var express = require('express');
var cors = require('cors')
const { postgraphile } = require("postgraphile");
const process = require("process");

async function run() {

    const app = express();

    app.set('view engine', 'ejs');
    app.use('/static', express.static('static'));

    app.use(cors())
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    app.use('/', require('./routes/pages'))
    app.use('/api', require('./routes/api'))


    app.use(
        // 'graphql',
        postgraphile(
            {
                database:  process.env.DATABASE_NAME,
                user:      process.env.DATABASE_USERNAME,
                password:  process.env.DATABASE_PASSWORD,
                host:      process.env.DATABASE_HOST,
                port:      process.env.DATABASE_PORT,
            },
            "product",
            {
                watchPg: true,
                graphiql: true,
                enhanceGraphiql: true,
            }
        )
    );


    app.listen(process.env.PORT, process.env.HOST);
    console.log(`${process.env.HOST}:${process.env.PORT} is opened`);

}

run().catch(error => {
    console.error("Fatal error", error)
})