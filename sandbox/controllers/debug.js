
const _ = require('lodash')
const knex = require('./knex')

const schema = 'apl_rf'

const PART = {
    gjs_css:         'gjs-css',
    gjs_assets:      'gjs-assets',
    gjs_styles:      'gjs-styles',
    gjs_components:  'gjs-components',
    gjs_html:        'gjs-html',
}

//https://github.com/apollographql/apollo-server/issues/1275
// app.use('/graphql', (req, res, next)=>{
//     const buildSchema = require('./backend/graphql/schema');
//     apolloServer.schema = buildSchema();
//     next();
// })
// apolloServer.applyMiddleware({ app, path: '/graphql' });

async function get_page_preview(form_key) {
    const page = await knex.withSchema(schema).select().table('pages').where({key: form_key}).first()

    const parts = await knex.withSchema(schema).select().table('parts').where({page_id: page.id})
    const parts_by_key = _.keyBy(parts, 'key')

    const html = parts_by_key[PART.gjs_html]
    const css = parts_by_key[PART.gjs_css]

    return {title: page.title, css: css.content, html: html.content}
}

module.exports = {get_page_preview};
