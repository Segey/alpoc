
const axios = require('axios')
const _ = require('lodash')

const axios_process = axios.create({
    baseURL: `http://camunda.local:8084/engine-rest/`,
})


async function run_action(data) {
    console.log('run_action', data)
    switch (data.type.value) {
        case 'kp':
            return await kp_run(data)
    }
}


async function kp_run(data) {
    switch (data.subType.value) {
        case 'start_event':
            return await kp_start(data)
    }
}

async function kp_start(data) {

    const mapType = apltype => {
        return apltype === 'integer' ? 'Integer'
            : "String"
    }

    const process_id = data['process-id'].value;

    const variables = {};
    const sys_fields = ['type','process-id','subName','subType','value']
    _(data).keys().difference(sys_fields).forEach(key => {
        variables[key] = {
            value : data[key].value,
            type: mapType(data[key].apltype)
        }
    })

    const props = {
        variables,
        businessKey : null
    }

    let url = `/process-definition/key/${process_id}/start`;
    console.log('starting process', url)
    return await axios_process.post(url, props)
}



module.exports = {run_action};
