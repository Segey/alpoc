const express = require('express');
const router = express.Router();

const {run_action} = require('../controllers/api')

router.post('/action', async (req, res) => {

    try {
        const data = req.body
        await run_action(data)
        res.sendStatus(200)
    } catch (error) {
        console.error('ACTION:', error.toString())
        res.status(500).send(error)
    }
})

module.exports = router;
