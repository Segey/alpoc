const express = require('express');
const router = express.Router();

const {get_page_preview} = require('../controllers/debug')

/**
 * Получить форму
 */
router.get('/:form_key', async (req, res) => {

    try {
        const form_key = req.params.form_key
        const options = await get_page_preview(form_key)
        console.log('options', options)
        res.render('main', options)

    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
})

module.exports = router;
