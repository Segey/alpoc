Appliner
========
Прототип 

### Общие требования для сервисов
    node js v13+
    JDK v16+

### Внешние сервисы:
#### camunda.local
    image: camunda/camunda-bpm-platform:latest
#### postgres.local
    image: postgres

backend-kmd
-----------
Конструктор модели данных

Конфигурация: .env

    HOST=0.0.0.0
    PORT=8093
    DATABASE_HOST=postgres.local
    DATABASE_PORT=5432
    DATABASE_NAME=appliner
    DATABASE_USERNAME=appliner
    DATABASE_PASSWORD=appliner


Запуск:

    cd backend-kmd
    npm install
    npm run run


backend-kp
-----------
Конструктор процессов

Конфигурация: application.yaml

    spring:
      datasource:
        url: jdbc:postgresql://postgres.local:5432/appliner
        username: appliner
        password: appliner
        hikari:
          maximum-pool-size: 5
          minimum-idle: 1
      jpa:
        properties:
          hibernate:
            jdbc:
              lob:
                non_contextual_creation: true
        hibernate:
          ddl-auto: validate
        open-in-view: false
      servlet:
        multipart:
          max-file-size: 20MB
          max-request-size: 200MB
    
    server:
      port: 8095
    app:
      camunda:
        url: http://camunda.local:8084/engine-rest


Запуск: 

    cd backend-kp
    ./mvnw spring-boot:run -Dspring-boot.run.profiles=local


backend-rf
-----------
Редактор форм

Конфигурация: .env

    HOST=0.0.0.0
    PORT=8094
    
    DATABASE_HOST=postgres.local
    DATABASE_PORT=5432
    DATABASE_NAME=appliner
    DATABASE_USERNAME=appliner
    DATABASE_PASSWORD=appliner


Запуск:

    cd backend-rf
    npm istall
    npm run run


sandbox
-----------
Сервис изображает готовый продукт

Конфигурация: .env
    
    HOST=0.0.0.0
    PORT=8090
    
    CAMUNDA_ENDPOINT=http://camunda.local:8084/engine-rest/
    
    DATABASE_HOST=postgres.local
    DATABASE_PORT=5432
    DATABASE_NAME=appliner
    DATABASE_USERNAME=appliner
    DATABASE_PASSWORD=appliner

Запуск:

    cd sandbox
    npm istall
    npm run run



webapp
-----------
Appliner, интерфейс пользователя

Конфигурация: .env
    
    VUE_APP_BASE_URL=/
    
    VUE_APP_KMD_URL=http://localhost:8093/
    VUE_APP_RF_URL=http://localhost:8094/
    VUE_APP_KP_URL=http://localhost:8095/
    
    VUE_APP_GRAPHILE_URL=http://localhost:8093/graphql
    
    VUE_APP_SANDBOX_URL=http://localhost:8090/
    

Запуск:

    cd webapp
    npm istall
    npm run serve


    
    
