package com.asin.kp.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ActionField {

    String type;
    String key;
    String name;
    String description;
}
