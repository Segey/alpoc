package com.asin.kp.service;

import com.asin.kp.controller.ProcessCatalogDTO;
import com.asin.kp.controller.ProcessCatalogVO;
import com.asin.kp.entity.ProcessCatalog;
import com.asin.kp.repository.ProcessCatalogRepository;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.transaction.Transactional;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.time.Instant;
import java.util.*;

@Service
public class ProcessCatalogService {

    @Autowired
    private ProcessCatalogRepository processCatalogRepository;

    public Integer save(ProcessCatalogVO vO) {
        ProcessCatalog bean = new ProcessCatalog();
        BeanUtils.copyProperties(vO, bean);
        bean = processCatalogRepository.save(bean);
        return bean.getId();
    }

    public void delete(String key) {
        processCatalogRepository.deleteByKey(key);
    }

    public ProcessCatalogDTO update(String key, ProcessCatalogVO vO) {
        ProcessCatalog bean = requireOne(key);
        BeanUtils.copyProperties(vO, bean);
        bean.setUpdatedAt(Date.from(Instant.now()));
        processCatalogRepository.save(bean);
        return toDTO(bean);
    }

    @Transactional
    public void updateContent(String key, String content) {
        ProcessCatalog processCatalog = requireOne(key);
        processCatalog.setContent(content);
        processCatalogRepository.save(processCatalog);
    }

    private ProcessCatalogDTO toDTO(ProcessCatalog original) {
        ProcessCatalogDTO bean = new ProcessCatalogDTO();
        BeanUtils.copyProperties(original, bean);
        return bean;
    }

    private ProcessCatalog requireOne(String key) {
        return processCatalogRepository.findByKey(key)
                .orElseThrow(() -> new NoSuchElementException("Resource not found: " + key));
    }

    public List<ProcessCatalog> getAll() {
        return processCatalogRepository.findAll();
    }

    public ProcessCatalogDTO getByKey(String key) {
        return toDTO(requireOne(key));
    }

    public String getContentByKey(String key) {
        return requireOne(key).getContent();
    }

}
