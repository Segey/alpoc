package com.asin.kp.service;

import com.asin.kp.utils.AXmlUtils;
import com.asin.kp.utils.CamundaUtils;
import com.asin.kp.entity.ProcessCatalog;
import com.asin.kp.repository.ProcessCatalogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.*;

@Slf4j
@Service
public class BuilderService {

    @Autowired
    AXmlUtils aXmlUtils;

    @Autowired
    CamundaUtils camundaUtils;

    @Autowired
    ProcessCatalogRepository processCatalogRepository;

    public void build(String key) throws IOException {
        ProcessCatalog process = processCatalogRepository.findByKey(key).orElseThrow(() -> new RuntimeException("Bad key " + key));

        log.info("\uD83D\uDE80 start building {}", key);

        String name = process.getName();
        String aXml = process.getContent();
        if (aXml == null) return;
        log.debug("Process content = {}", aXml);

        String cXml = aXmlUtils.toCXml(aXml);

//        FileCopyUtils.copy(aXml, new FileWriter("C:\\Users\\asin\\work\\Appliner\\appliner-pg2\\backend-kp\\src.xml"));
//        FileCopyUtils.copy(cXml, new FileWriter("C:\\Users\\asin\\work\\Appliner\\appliner-pg2\\backend-kp\\res.xml"));

        camundaUtils.deploy(key, name, cXml);
    }

}
