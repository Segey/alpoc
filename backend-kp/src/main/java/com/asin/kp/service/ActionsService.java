package com.asin.kp.service;

import com.asin.kp.entity.ProcessCatalog;
import com.asin.kp.service.dto.Action;
import com.asin.kp.service.dto.ActionField;
import com.asin.kp.utils.AXmlUtils;
import com.asin.kp.utils.FormData;
import com.asin.kp.utils.FormDataType;
import com.asin.kp.utils.FormField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActionsService {

    @Autowired
    ProcessCatalogService processCatalogService;

    @Autowired
    AXmlUtils aXmlUtils;

    public List<Action> getAll() {
        List<Action> res = new ArrayList<>();

        List<ProcessCatalog> processes = processCatalogService.getAll();
        for (ProcessCatalog process : processes) {

            String content = process.getContent();
            List<FormData> forms = aXmlUtils.extractForms(content);
            for (FormData form : forms) {

                Action action = new Action();

                action.setKey(process.getKey());
                action.setType("kp");
                action.setName(process.getName());

                action.setSubKey(form.getContainerId());
                action.setSubType(form.getType().name().toLowerCase());

                String subname = form.getType() == FormDataType.START_EVENT
                        ? "Запуск"
                        : String.format("Задача '%s'", form.getContainerName());
                action.setSubName(subname);

                for (FormField formField : form.getFormField()) {
                    ActionField field = new ActionField(formField.getType(), formField.getKey(), formField.getName(), formField.getDescription());
                    action.getFields().add(field);
                }
                res.add(action);

            }

        }

        return res;
    }
}
