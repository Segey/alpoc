package com.asin.kp.service.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Action {

    protected String key;
    protected String type;
    protected String name;

    protected String subKey;
    protected String subType;
    protected String subName;

    protected List<ActionField> fields = new ArrayList<>();

}
