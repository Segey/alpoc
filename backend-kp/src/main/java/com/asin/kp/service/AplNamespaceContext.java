package com.asin.kp.service;

import javax.xml.namespace.NamespaceContext;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AplNamespaceContext implements NamespaceContext {

    Map<String, String> nss = new HashMap<>();

    public AplNamespaceContext() {
        nss.put("bpmn", "http://www.omg.org/spec/BPMN/20100524/MODEL");
        nss.put("appliner", "http://appliner");
    }

    @Override
    public String getNamespaceURI(String prefix) {
        return nss.getOrDefault(prefix, null);
    }

    @Override
    public String getPrefix(String namespaceURI) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<String> getPrefixes(String namespaceURI) {
        throw new UnsupportedOperationException();
    }
}
