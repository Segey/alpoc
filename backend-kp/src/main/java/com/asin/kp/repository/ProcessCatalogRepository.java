package com.asin.kp.repository;

import com.asin.kp.controller.ProcessCatalogDTO;
import com.asin.kp.entity.ProcessCatalog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface ProcessCatalogRepository extends JpaRepository<ProcessCatalog, Integer>, JpaSpecificationExecutor<ProcessCatalog> {

    Optional<ProcessCatalog> findByKey(String key);

    void deleteByKey(String key);
}