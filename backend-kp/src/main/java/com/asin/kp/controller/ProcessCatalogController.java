package com.asin.kp.controller;

import com.asin.kp.entity.ProcessCatalog;
import com.asin.kp.service.ProcessCatalogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@CrossOrigin
@Api(tags = "Каталог процессов")
@Validated
@RestController
@RequestMapping("/processes")
public class ProcessCatalogController {

    @Autowired
    private ProcessCatalogService processCatalogService;

    @PostMapping
    @ApiOperation("Save Каталог процессов")
    public String save(@Valid @RequestBody ProcessCatalogVO vO) {
        return processCatalogService.save(vO).toString();
    }

    @GetMapping("/{key}")
    @ApiOperation("Retrieve by Key Каталог процессов")
    public ProcessCatalogDTO getByKey(@Valid @NotNull @PathVariable("key") String key) {
        return processCatalogService.getByKey(key);
    }

    @GetMapping("/{key}/content")
    @ApiOperation("Retrieve by Key Каталог процессов")
    public String getContentByKey(@Valid @NotNull @PathVariable("key") String key) {
        return processCatalogService.getContentByKey(key);
    }

    @DeleteMapping("/{key}")
    @ApiOperation("Delete Каталог процессов")
    public void delete(@Valid @NotNull @PathVariable("key") String key) {
        processCatalogService.delete(key);
    }


    @PutMapping("/{key}")
    @ApiOperation("Update Каталог процессов")
    public ProcessCatalogDTO update(@Valid @NotNull @PathVariable("key") String key, @Valid @RequestBody ProcessCatalogVO vO) {
        return processCatalogService.update(key, vO);
    }

    @PutMapping(value = "/{key}/content")
    @ApiOperation("Update Каталог процессов")
    public void updateContent(@Valid @NotNull @PathVariable("key") String key, @Valid @RequestBody String content) {
        processCatalogService.updateContent(key, content);
    }

    @GetMapping
    @ApiOperation("Retrieve by query Каталог процессов")
    public List<ProcessCatalog> query() {
        return processCatalogService.getAll();
    }
}
