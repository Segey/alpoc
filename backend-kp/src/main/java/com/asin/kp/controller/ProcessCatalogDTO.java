package com.asin.kp.controller;


import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel("Каталог процессов")
public class ProcessCatalogDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;

    private String name;

    private String key;

    private String description;

    private Date createdAt;

    private Date updatedAt;

}
