package com.asin.kp.controller;

import com.asin.kp.entity.ProcessCatalog;
import com.asin.kp.service.BuilderService;
import com.asin.kp.service.ProcessCatalogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.List;

@CrossOrigin
@Api(tags = "Каталог процессов 2")
@Validated
@RestController
@RequestMapping("/builder")
public class BuilderController {

    @Autowired
    private BuilderService builderService;

    @PostMapping("/{key}")
    @ApiOperation("Update Каталог процессов")
    public void update(@Valid @NotNull @PathVariable("key") String key) throws IOException, TransformerException {
        builderService.build(key);
    }

}
