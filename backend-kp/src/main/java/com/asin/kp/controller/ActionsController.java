package com.asin.kp.controller;

import com.asin.kp.service.ActionsService;
import com.asin.kp.service.dto.Action;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@Api(tags = "Каталог действий")
@Validated
@RestController
@RequestMapping("/actions")
public class ActionsController {

    @Autowired
    private ActionsService actionsService;

    @GetMapping
    @ApiOperation("Получение списка действий")
    public List<Action> get()  {
        return actionsService.getAll();
    }

}
