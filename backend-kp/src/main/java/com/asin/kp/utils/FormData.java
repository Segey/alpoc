package com.asin.kp.utils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class FormData {

    String containerId;
    String containerName;
    FormDataType type;

    @JacksonXmlProperty
    String name;
    @JacksonXmlProperty
    String key;

    @JacksonXmlElementWrapper(useWrapping = false)
    List<FormField> formField = new ArrayList<>();
}

