package com.asin.kp.utils;

import com.asin.kp.service.AplNamespaceContext;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AXmlUtils {

    @Value("classpath:transform.xslt")
    private Resource resource;

    @SneakyThrows
    public String toCXml(String aXml) {

        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(resource.getInputStream());
//        Source xslt = new StreamSource(new FileInputStream("C:\\Users\\asin\\work\\Appliner\\appliner-pg2\\backend-kp\\src\\main\\resources\\transform.xslt"));

        Transformer transformer = factory.newTransformer(xslt);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        Source xml = new StreamSource(new StringReader(aXml));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        transformer.transform(xml, new StreamResult(byteArrayOutputStream));

        String cXml = byteArrayOutputStream.toString();
        log.debug("Transform result = {}", cXml);

        return cXml;
    }

    public static void main(String[] args) throws IOException {

        String fiel = "C:\\Users\\asin\\work\\Appliner\\appliner-pg2\\backend-kp\\src.xml";
        String content = FileCopyUtils.copyToString(new FileReader(fiel));

        AXmlUtils aXmlUtils = new AXmlUtils();
        List<FormData> forms = aXmlUtils.extractForms(content);
        for (FormData form : forms) {
            System.out.println("form = " + form);
        }
    }

    @SuppressWarnings("CollectionAddAllCanBeReplacedWithConstructor")
    @SneakyThrows
    public List<FormData> extractForms(String processXml) {

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);

        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        InputStream fileIS = new ByteArrayInputStream(processXml.getBytes());
        Document xmlDocument = builder.parse(fileIS);

        XPath xPath = XPathFactory.newInstance().newXPath();
        xPath.setNamespaceContext(new AplNamespaceContext());

        List<FormData> formsData = new ArrayList<>();

        XPathExpression startEventEx = xPath.compile("//bpmn:startEvent");
        formsData.addAll(extractForms(startEventEx, xmlDocument, FormDataType.START_EVENT));

        XPathExpression userTaskEx = xPath.compile("//bpmn:userTask");
        formsData.addAll(extractForms(userTaskEx, xmlDocument, FormDataType.USER_TASK));

        return formsData;
    }

    @SneakyThrows
    public List<FormData> extractForms(XPathExpression xpath, Node node, FormDataType type) {

        Transformer transformer = TransformerFactory.newInstance().newTransformer();

        XPath xPath = XPathFactory.newInstance().newXPath();
        xPath.setNamespaceContext(new AplNamespaceContext());

        XPathExpression formDataEx = xPath.compile("bpmn:extensionElements/appliner:formData");

        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        XmlMapper xmlMapper = new XmlMapper(xmlInputFactory);

        List<FormData> formsData = new ArrayList<>();

        NodeList nodeList = (NodeList) xpath.evaluate(node, XPathConstants.NODESET);
        for (int i = 0; i < nodeList.getLength(); i++) {

            Node formContainingNode = nodeList.item(i);
            Element el = (Element) formContainingNode;
            String id = el.getAttribute("id");
            String name = el.getAttribute("name");

            Node formNode = (Node) formDataEx.evaluate(formContainingNode, XPathConstants.NODE);

            if (formNode != null) {
                ByteArrayOutputStream sw = new ByteArrayOutputStream();
                transformer.transform(new DOMSource(formNode), new StreamResult(sw));

                FormData formData = xmlMapper.readValue(sw.toByteArray(), FormData.class);

                formData.setContainerId(id);
                formData.setContainerName(name);
                formData.setType(type);
                formsData.add(formData);

            } else {
                FormData formData = new FormData();
                formData.setContainerId(id);
                formData.setContainerName(name);
                formData.setType(type);
                formsData.add(formData);
            }

        }

        return formsData;
    }


}
