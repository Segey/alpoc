package com.asin.kp.utils;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class DeploymentResponse {
    String id;
    String name;
    String source;
    String deploymentTime;
    String deployedCaseDefinitions;
    String deployedDecisionDefinitions;
    String deployedDecisionRequirementsDefinitions;
    Map<String, DeployedProcessDefinitions> deployedProcessDefinitions = new HashMap<>();
}

@Data
class DeployedProcessDefinitions {
    String id;
    String key; //: "Process_0eww0iy",
    String category; //: "http://bpmn.io/schema/bpmn",
    String description; //: null,
    String name; //: "Проверка значения",
    Integer version; //: 1,
    String resource; //: "test2.bpmn",
    String deploymentId; //: "8d523f5c-7d17-11ec-a24d-0242c0a86004",
//    diagram; //: null,
    Boolean suspended; //: false,
//    tenantId; //: null,
//    versionTag; //: null,
//    historyTimeToLive; //: null,
    Boolean startableInTasklist; //: true

}
