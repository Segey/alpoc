package com.asin.kp.utils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

@Data
public
class FormField {
    @JacksonXmlProperty
    String type;
    @JacksonXmlProperty
    String key;
    @JacksonXmlProperty
    String name;
    @JacksonXmlProperty
    String description;
}
