package com.asin.kp.utils;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Slf4j
@Service
public class CamundaUtils {

    @Value("${app.camunda.url}")
    URI camunda;

    @SneakyThrows
    public void deploy(String key, String name, String data) {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUri(camunda).pathSegment("deployment", "create").build().toUri();
        log.info("Deploy to {}", uri);

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("deployment-name", name);
        parts.add("deployment-source", "Appliner");
        parts.add("enable-duplicate-filtering", true);
        parts.add("deploy-changed-only", true);

        Resource resource = new ByteArrayResource(data.getBytes()) {
            @Override public String getFilename() { return key+".bpmn"; }
        };
        parts.add("data", resource);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(parts, headers);

        ResponseEntity<DeploymentResponse> response = restTemplate.exchange(uri, HttpMethod.POST, requestEntity, DeploymentResponse.class);

        DeploymentResponse body = response.getBody();

        log.info("Deployed processes = {} ", body.deployedProcessDefinitions);
    }

}
