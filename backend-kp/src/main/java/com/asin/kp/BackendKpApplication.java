package com.asin.kp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendKpApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendKpApplication.class, args);
	}

}
