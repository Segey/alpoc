<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:appliner="http://appliner"
                xmlns:camunda="http://camunda.org/schema/1.0/bpmn"
                xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL"
>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="bpmn:process">
        <bpmn:process id="{@id}" name="{@name}" isExecutable="true">
            <xsl:apply-templates/>
        </bpmn:process>
    </xsl:template>

    <xsl:template match="appliner:formData">
        <camunda:formData formKey="{@key}">
            <xsl:apply-templates/>
        </camunda:formData>
    </xsl:template>

    <xsl:template match="appliner:formField">
        <camunda:formField id="{@name}" label="{@name}" type="string" />
    </xsl:template>

</xsl:stylesheet>