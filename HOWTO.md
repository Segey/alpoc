Appliner
========
# Cammunda connect data
- ip = current ip
- port = 8080

# Postgress connect data
- ip = current ip
- port = 5432


### How to install on Ubuntu 21.10
## install node
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt update
sudo apt install -y nodejs

## install other
sudo apt install default-jre -y
sudo snap install liquibase

### Prepare and run
## Postgress
- cd postgress
- change volumes path
- docker-compose up

# Create db and tabels
pgadmin: localhost:5050
login: admin@xxx.com
pass: admin 
- create camunda database
 - add user camunda  | CREATE USER camunda WITH PASSWORD 'camunda'; 
- create appliner database
 - SQL exec backend-rf/init.sql to appliner database

## Camunda
- cd camunda
- change DB_URL path to postgres
- docker-compose up

## backend-kmd
- cd backend-kmd
- '.env' file change change postgress connect data
- npm i
- npm run run

## backend-kp
- cd backend-kp
- 'src/main/resources/application.yaml' change postgress connect data and camunda
- chmod +x ./mvnw
- ./mvnw spring-boot:run -Dspring-boot.run.profiles=local

## backend-rf
- cd backend-rf
- '.env' file change change postgress connect data
- npm i
- npm run run

## sandbox
- cd sandbox
- '.env' file change change camunda and postgress connect data
- npm i
- npm run run

## webapp
- cd webapp
- npm i
- npm run serve 
